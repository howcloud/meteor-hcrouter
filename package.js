Package.describe({
	name: 'howcloud:router',
	summary: 'Routing designed specifically around HowCloud/React needs. Implements a lot of the base Iron libraries/logic but without the fluff we do not need from Iron:Router',
	version: "0.1.0"
});

/**

Ideally this needs further abstraction to remove the dependencies on HowCloudness and make it a pure React based routing system for Meteor apps

**/

Npm.depends({
	'body-parser': '1.8.1',
	'cookie-parser': '1.3.3'
});

Package.on_use(function (api) {
	/** Set Dependencies **/

	api.use('jquery');
	api.use('underscore');

	api.use('meteor'); // for dynamic scoping with environment variables
	api.use('webapp', 'server'); // for adding connect middleware
	api.use('deps', 'client'); // for handling URL changes using the dependency tracker

	api.use('gadicohen:inject-initial'); // for react stuff
	api.use('howcloud:react'); // for rendering components
	api.use('howcloud:react-deps');
	api.use('howcloud:react-build');

	api.use('howcloud:accounts-saml', ['client', 'server'], {weak: true}); // for saml url generation if controller auth system suggests we need it (in which case must be loaded in)
	api.use('accounts-base', ['server', 'client'], {weak: true}); // for server side login system

	// Iron namespace and utils [LOADED IN BY OTHER PACKAGES IF NEEDED, SURELY]
	//api.use('iron:core@1.0.4');
	//api.imply('iron:core');
	
	// Iron packages
	api.use('iron:url@1.0.4'); // client and server side url utilities and compiling
	api.use('iron:location@1.0.4'); // for reactive urls and pushState in the browser

	/** Add Files **/

	// Collections

	// User system (to enable server side login via cookie)
	api.add_files("lib/collections/users_client.js", "client");
	api.add_files("lib/collections/users_server.js", "server");

	// JS

	api.add_files('lib/HC_client.js', 'client');
	api.add_files('lib/HC_server.js', 'server');
	api.export('HC'); // TODO: Ship this off to another package if the HC scoped variable list grows

	api.add_files('lib/components/SiteController.jsx');
	api.add_files('lib/components/DelegateRender.jsx');

	api.add_files('lib/route.js');
	api.add_files('lib/router.js');

	api.add_files('lib/route_server.js', 'server');
	api.add_files('lib/router_server.js', 'server');

	api.add_files('lib/route_client.js', 'client');
	api.add_files('lib/router_client.js', 'client');

	api.add_files('lib/global_router.js');

	// CSS

	api.add_files('css/containers.css', 'client');

	/** Export **/

	api.export('Router');
	api.export('RequestVars'); // have to export so that meteor-react can inject initial variables into this response

	api.export('DelegateRender'); // component
	api.export('delegateRender'); // function

});