/**
 * 
 */

/**

SiteController
All controllers are loaded and routed through this one
If we want to move from one controller to the next it is done through this (shows loading progress as we get meteor state)
Also used to escape stacking contexts and provide overlay containers
In theory only one will ever be active during any single window viewing session, it is the entire container of the site

<RouterController>
	
</RouterController>

Note that the access checks/redirection etc does not need to be checked on the client side
It's purely a server side issue (at least for now)
In theory we should never be directed to pages which we don't have access to from the client side

**/

var renderDelegateKey = 0; // used to set a unique key on all render delegates when they are added
						   // this means that we keep a unique lookup of delegated renders to keys

SiteController = React.createClass({
	
	/** Component Properties **/

	propTypes: {
		//controller: React.PropTypes.component, // todo: this will probably move to state and we'll browse by calling functions on this controller to switch the state/push new loading controllers when we get around to doing this properly and less as just as hack to allow stack context escapes
		// ^- for some reason proptype of component doens't seem to be working right
		controllerProps: React.PropTypes.object,
	},

	/** State **/

	getInitialState: function () {
		return {
			outerRenderDelegates: {},
			blur: 0,
		}
	},

	/** Mixins **/

	mixins: [
		React.addons.PureRenderMixin
	],

	/** Blur **/
	// Allows us to blur the current page (ie when we are rendering a modal etc)
	// We model this like an alloc count - ie can be called multiple times and will only stop bluring when all blur calls are unblurred

	blur: function () {
		this.setState({blur: this.state.blur + 1});
	},
	unblur: function () {
		var _blur = this.state.blur - 1;
		if (_blur < 0) _blur = 0;

		this.setState({blur: _blur});
	},

	/** Freeze Body Scroll **/
	// Allows us to freeze the scroll location of the body and disable the user's ability to scroll it
	// Used by modals, and other ui effects such as mobile menu etc

	bodyScroll: 0, // works like an alloc count like this.state.blur as it could be stacked up multiple times in theory (mobile menu triggered + login dialogue for example)
				   // we could implement a system where by render delegates can freeze the scroll of other render delegates... but not really necessary at the moment

	savedBodyScrollTop: 0,

	freezeBodyScroll: function () {
		if (++this.bodyScroll === 1) { // wasn't previously frozen - so do the freeze
			this.savedBodyScrollTop = jQuery('body').scrollTop();

			var $doc = jQuery('html, body');
			$doc.css({'overflow': 'hidden'});
		}
	},
	unfreezeBodyScroll: function () {
		if (!--this.bodyScroll) { // => unfreeze as 'alloc' count is now 0
			var $doc = jQuery('html, body');

			$doc.css({ 'overflow': 'initial' });
			jQuery('body').scrollTop(this.savedBodyScrollTop);
		}
	},

	/** 

	Render Delegates
	A hack to escape stack contexts and render stuff at the outer most level of a page's DOM (outside even the confines of the current controller)
	Useful for escaping stacking contexts to do overlays, for example

	**/

	useRenderDelegate: function (_delegate, _callback) {
		if (_delegate._renderDelegateKey) return _delegate._renderDelegateKey; // already rendering

		var key = ++renderDelegateKey;

		_delegate._renderDelegateKey = key;
		this.state.outerRenderDelegates[key] = _delegate; // naughty in place mutation of state

		this.forceUpdate(_callback);

		return key;
	},
	removeRenderDelegate: function (_delegate, _callback) {
		setTimeout(function () {
			delete this.state.outerRenderDelegates[_delegate._renderDelegateKey];
			delete _delegate._renderDelegateKey;

			this.forceUpdate(_callback);
		}.bind(this), 0); // Hacky fix to escape setting state during a render
	},

	getDelegateNode: function (_delegateKey) {
		if (!this.refs) return null;

		var delegateContainer = this.refs['delegate_' + _delegateKey];
		if (!delegateContainer) return null;

		// return delegateContainer._renderedChildren['.0']; // MAJOR HACK which worked in <0.13 but not anymore, now see below for updated version:

		if (!delegateContainer._reactInternalInstance) return null;
		if (!delegateContainer._reactInternalInstance._renderedComponent) return null;
		if (!delegateContainer._reactInternalInstance._renderedComponent._renderedChildren) return null;
		if (!delegateContainer._reactInternalInstance._renderedComponent._renderedChildren['.0']) return null;

		return delegateContainer._reactInternalInstance._renderedComponent._renderedChildren['.0']._instance; // hacked in - deduced via console... no idea how/why this is like it is... but shoul hold constant at least for SiteController
	},

	/** React **/

	componentDidMount: function () {
		HC.SiteControllerInstance = this; // assuming this is on the client, else by setting something like this we could do damage, unless we wrapped it and used it as an environment variable on the server side (this is the best solution eventually)
	},

	/** Render **/

	render: function () {

		/** Styles **/

		/* Container Container */

		var containerContainer_classes = classNames({
			'container-container': true,
			'container-container-blur': this.state.blur
		});

		/** Render **/

		return (
			<div className="outer-container">
				<div className={containerContainer_classes}>
					{this.props.controller ? React.createElement(this.props.controller, this.props.controllerProps): null}
				</div>

				<TimeoutTransitionGroup transitionName="render-delegate" enterTimeout={200} leaveTimeout={200} appearTimeout={200}>
					{_.map(this.state.outerRenderDelegates, function (render, index) {
						// TODO: Different classes on these divs depending on the effect we want when this div enters as a render delegate

						return (
							<div ref={'delegate_'+index} key={index} className="render-delegate container">
								{render()}
							</div>
						);
					})}
				</TimeoutTransitionGroup>
			</div>
		);
	},

});
