/**
 * 
 */

/** 

delegateRender function
Delegates a render function to the HC.SiteControllerInstance rather than managing it in a component's render function
Used for prompts, modals etc

options = {
    render: function
}

**/

delegateRender = function (options) {
    options = options || {};

    var delegation = {}
    delegation.props = options.props;

    delete options.props;
    options.render = options.render || function () { return null; }

    var onCancel = options.onCancel;
    delete options.onCancel;

    /** Create the component to describe this delegation **/

    var DelegateComponent = React.createClass(_.extend({
        mixins: [React.addons.PureRenderMixin],

        cancel: function () {
            delegation.cancel();
        }
    }, options));

    /** Create the delegation control level API **/

    delegation.render = function () {
        return (<DelegateComponent {...delegation.props} />);
    }.bind(delegation);

    delegation.cancel = function () {
        HC.SiteControllerInstance.removeRenderDelegate(delegation.render);
        if (onCancel) onCancel();
    }.bind(delegation);

    delegation.forceUpdate = function () {
        HC.SiteControllerInstance.forceUpdate();
    }.bind(delegation);

    delegation.setProps = function (props) {
        _.extend(delegation.props, props);
        this.forceUpdate();
    }.bind(delegation);

    /*********/

    // Copied from DelegateRender component
    // uses recursion to only try and actually get a render delegate rendered once we have a ref to HC.SiteControllerInstance
    // todo: factor out into a single method

    (function doDelegation () {
        if (HC.SiteControllerInstance) {
            if (this.delegateKey) {
                HC.SiteControllerInstance.forceUpdate();
            } else {
                // this.delegateKey = HC.SiteControllerInstance.useRenderDelegate(this.render_delegate, this.props.onMount);
                HC.SiteControllerInstance.useRenderDelegate(delegation.render);
            }
        } else {
            setTimeout(function () {
               doDelegation();
            }.bind(this), 0);
        }
    })();

    /*********/

    // HC.SiteControllerInstance.useRenderDelegate(delegation.render);

    return delegation;
}

/*

DelegateRender Component
Allows us to delegate the rendering of child elements into another instance of a component
Right now this assumes HC.SiteControllerInstance but in the future could be anything which implements a delegatable interface (although not that many other user cases apart from to escape z stacking)
The useful thing about abstracting this into a component is that we can render this and children and update on prop changes as normal without any hacks

*/

DelegateRender = React.createClass({

	/** Props/State **/

	propTypes: {
		onMount: React.PropTypes.func, // callback for when the initial render has actually happened
	},
	// getDefaultProps: function () {
	// 	return {
		
	// 	}
	// },

	/** Mixins **/

	mixins: [
		React.addons.pureRenderMixin,
	],

	/** Update **/

	delegateKey: false, // the render delegate key for this delegation (allows us to access the ref for the component on the SiteControllerInstance)

	update: function () {
		if (HC.SiteControllerInstance) {
			if (this.delegateKey) {
				HC.SiteControllerInstance.forceUpdate();
			} else {
				this.delegateKey = HC.SiteControllerInstance.useRenderDelegate(this.render_delegate, this.props.onMount);
			}
		} else {
			setTimeout(function () {
				this.update(); // ie need to do it after initial mount when HC.SiteControllerInstance is set
			}.bind(this), 0);
		}
	},

	/** Node **/

	getNode: function () {
		if (!this.delegateKey) return null;

		return HC.SiteControllerInstance.getDelegateNode(this.delegateKey);
	},

	/** React **/

	componentWillMount: function() {
		if (Meteor.isClient) this.update(); // if we can, if not this is equivalent to doing it on componentDidMount anyway
	},
	componentWillUnmount: function () {
		HC.SiteControllerInstance.removeRenderDelegate(this.render_delegate);
		this.delegateKey = false;
	},
	componentWillUpdate: function(nextProps, nextState) {
		this.update();
	},

	/** Render **/

	render: function () {
		return null; // the component does not render anything directly here
	},

	render_delegate: function () {
		return this.props.children; // we render children when delegate function is called
	},

});