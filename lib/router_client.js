Router.prototype.start = function () {

	Deps.autorun(function () {

		/** Get route for loc **/

		var _location = Iron.Location.get(); // this is essentially the dep we are monitoring
											 // note that sometimes for some reason Iron location doesn't seem to work properly - doesn't return a host but has a path specified, so we use the main location variable to monitor host now

		/** Find Route **/

		var params = {};
		var route = this.findRoute(_location.path, location.host);

		if (route) {
			params = route.getUrlParams(_location.href);
		} else {
			route = this.findRoute('/404'); // get 404 page [hack]
		}

		/** Evaluate the route **/

		if (!route.eval(_location.path, params)) {
			window.location = _location.path;
		} else {
			jQuery('html, body').scrollTop(0); // force the page scroll back to the top, especially useful on pages which don't switch controller and may even render instantly

			if (window.__insp) window.__insp.push(["virtualPage"]); // HACK: trigger an inspectlet virtual page view
		}

	}.bind(this));

}