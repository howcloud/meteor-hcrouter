var cookieParser = Npm.require('cookie-parser');
var bodyParser = Npm.require('body-parser');

Router.prototype.start = function () {

	WebApp.connectHandlers // add handlers into the connect middleware for requests made into the server, hijacks Meteor's default functionality
	.use(cookieParser()) // gets us cookie content
	.use(bodyParser.urlencoded({extended: true})) // gets us post/get content
	.use(bodyParser.json()) // gets us json encoded post/get content, used for API mainly
	.use(function (req, res, next) {

		/** Find Route **/

		var params = {};
		var route = this.findRoute(req.url, req.headers.host);

		if (route) {
			params = route.getUrlParams(req.url);
		} else {
			route = this.findRoute('/404'); // get 404 page [hack]
		}

		/** Evaluate the route **/

		if (route) {
			route.eval(params, req, res, next);	
		} else {
			res.writeHead(404);
			res.end();
		}

	}.bind(this));

}