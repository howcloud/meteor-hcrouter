/** Update Cookies Whenever Login Changes **/
// allows us to login to an account whenever we make a request to the server for react based rendering of controllers
// todo: this could be separated into a separate package if we wanted and added as a step for our webserver request cycle but we put it here for now as this is part of core react meteor functionality for all our use cases right now

if (Meteor.userId) {
    Meteor.startup(function () {
        Deps.autorun(function () {
            var userId = Meteor.userId(); // we need to get this just to ensure that this function is registered as reactive

            var loginToken = Meteor._localStorage.getItem('Meteor.loginToken');
            var userId = Meteor._localStorage.getItem('Meteor.userId');

            createCookie('Meteor_loginToken', loginToken);
            createCookie('Meteor_userId', userId);
        });
    });
}

var createCookie = function (name,value,days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        var expires = "; expires="+date.toGMTString();
    }
    else var expires = "";
    document.cookie = name+"="+value+expires+"; path=/"; // domain= sets cookies across all apps, do we necessarily want this for all apps?
}

// unused cookie functions

// var readCookie = function (name) {
//     var nameEQ = name + "=";
//     var ca = document.cookie.split(';');
//     for(var i=0;i < ca.length;i++) {
//         var c = ca[i];
//         while (c.charAt(0)==' ') c = c.substring(1,c.length);
//         if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
//     }
//     return null;
// }

// var eraseCookie = function(name) {
//     createCookie(name,"",-1);
// }