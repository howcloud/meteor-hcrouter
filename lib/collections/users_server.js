/** Meteor User/UserId Wrap **/
// Enables us to always use in server side rendered react components
// It's non reactive but that doesn't matter as server side rendering is a one off non reactive activity anyway
// Defining this just stops us having to set custom code for getting meteor state related to the current user on the server side

if (Meteor.user) {
	Meteor.user = _.wrap(Meteor.user, function (meteorUser) {
		var _rv = RequestVars ? RequestVars.get() : null;
		if (_rv && _rv.req) return _rv.user;

		try {
			return meteorUser();
		} catch (err) {
			return null; // basically we not let Meteor.user() throw an error anymore (ie this will catch points where we are using this in the wrong place (ie using Meteor.User() in a publish function) so need to be aware that this error will not trigger anymore)
						 // reason we do this is because we sometimes want to be able to use Meteor.user etc or function which call it within API functions which do not have RequestVars but still want to cleanly call this function
		}
	});
}

if (Meteor.userId) {
	Meteor.userId = _.wrap(Meteor.userId, function (meteorUserId) {
		var _rv = RequestVars ? RequestVars.get() : null;
		if (_rv && _rv.req) return _rv.user_id;

		try {
			return meteorUserId();
		} catch (err) {
			return null; // see Meteor.user explanation above
		}
	});
}