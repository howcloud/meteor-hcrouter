RequestVars = new Meteor.EnvironmentVariable(); // holds data for a request so that we can store the current logged in user for a particular request, for example

Route.prototype.eval = function (params, req, res, next) {
	
	var Fiber = Npm.require('fibers');

	Fiber(function() {

		this._eval(params, req, res, next);

	}.bind(this)).run();
}

Route.prototype._eval = function (params, req, res, next) {

	if (this.action) {

		if (this.action(params, req, res, next)) return; // if we didn't deal with the request in the custom action then carry on with standard request

	} else if (this.redirect) {

		res.writeHead(302, {
			'location': typeof this.redirect === 'function' ? this.redirect(params) : this.redirect
		});
		res.end();
		
 	/*} else if (this.reroute) {

		this.reroute(params).eval(parmas, req, res, next);

	}*/

	} else if (this.rewrite) {

		var rewrite = this.getRewriteData(params);
		rewrite.route.eval(rewrite.params, req, res, next);

	} else if (this.controllerComponent) {

		/** User Authentication **/
		// hacky way to log the user back in on the server side (todo: clean up, abstract for tighter integration with accounts system)
		// TODO: Move to a cookie-login package or something

		var _user = null;
		if (typeof Accounts !== 'undefined') {
			if (req.cookies && req.cookies.Meteor_loginToken) {
				var _user = Meteor.users.findOne({
					"services.resume.loginTokens.hashedToken": Accounts._hashLoginToken(req.cookies.Meteor_loginToken)
				}, {
					fields: {username: 1, email: 1, profile: 1, guest: 1}
				});

				if (_user != null) { // validate if we found a user login 
					if (req.cookies.Meteor_userId != _user._id) _user = null;
				}
			}
		}

		/** Setup Request Vars **/

		var requestVars = {
			user: _user,
			user_id: _user ? _user._id : null,

			res: res, // needed by react package to inject initial on correct request/response
			req: req,

			params: params
		}

		RequestVars.withValue(requestVars, function () {

			/** Check User Access to Controller [if an auth check is defined] **/
			// We run this within defined RequestVars so we have access to call Meteor.userId etc in usercheckCanAccess

			var checkCanAccess = this.userCanAccess(requestVars.user_id, params);

			if (checkCanAccess) {
				if (typeof checkCanAccess === 'object') { // ie not a simply yes (unless .access = true which is the old way of saying yes, can now just return true)
					if (!checkCanAccess.access) {
						var redirect = null;

						if (checkCanAccess.missing) { // => 404
							// happy to just render this one up and send it down to the client
							// will render the same, it's genuinely missing
						} else if (checkCanAccess.redirect) { // can't think of a case where we'd directly set this but have it here just in case
							redirect = checkCanAccess.redirect;
						} else if (checkCanAccess.authedLocation && (_user && !_user.isGuest())) {
							// authedLocation is a location which the user is guaranteed to be able to access
							// we only redirect to this if the user is actually logged into another account which does not have access to this URL

							redirect = checkCanAccess.authedLocation;
						} else {
							// else either there's no authed location or we are just not logged into an actual account
							// action = if a saml connection is returned (ie for unis) then we redirect to that
							// 			else: redirect to the login page and let them login via that 

							if (checkCanAccess.samlIdp) {
								redirect = Meteor.loginWithSamlUrl(checkCanAccess.samlIdp, HC.web_url() + req.url.substring(1));
							} else {
								redirect = HC.web_url() + 'login?'+toQueryString({
									continue: req.url,
									requireLogin: 1
								});
							}
						}

						if (redirect) {
							res.writeHead(302, {'location': redirect});
							res.end();

							return;
						}
					}
				}
			}

			/******************************************************************************************************************************************/

			/** Render **/

			// NOTE: we need to render this in the same way as if we were doing it on the client
			var render = React.createElement(SiteController, {
				controller: this.controllerComponent, 
				controllerProps: requestVars.params
			});

			// To handle whether or not we are authed we can render the component and pass a callback to flag so
			// On the server side we can make sure this is called at the right point to trigger a flag if not authed
			// In this case we can pull and immediately provide/react to the necessary conditions to authorise the user to access this page
			// (unless it is just a plain 404 in which case don't worry)

			/* Body */

			var renderedComponent;

			try {
				renderedComponent = ReactDOMServer.renderToString(render);
			} catch (err) {
				// this is a hacky way to make sure that the document error does not crash our script and instead we just retry the request
				// TODO: limit number of times a request should be retried?
				// 13/11: Commented this out as we have now upgraded to React >0.14.2 which should not suffer from this error in the first instamce

				// if (err && String(err) === 'ReferenceError: document is not defined') {
				if (err && String(err).indexOf('_currentElement') !== -1) {
					console.log("GOT _currentElement ERROR - redirecting to retry request");
					// this._eval(params, req, res, next);

					res.writeHead(302, {'location': HC.web_url() + req.url.substring(1)});
					res.end();
					
					return;
				} else {
					// got an error which isn't the one we want to just ignore - so throw it

					console.log("Got error during request");
					console.log(req.url);
					console.log(err.stack);
					
				}
			}

			Inject.rawBody('reactServerRender', '<div id="fb-root"></div><div id="react-root">'+renderedComponent+'</div>', res);
			
			/* Meta Tag Info */

			var metaTags = this.getMetaTags(params);
			if (metaTags) {
				metaTags = _.map(metaTags, function (content, property) {
					return '<meta property="'+property+'" content="'+content+'" />';
				});

				Inject.rawHead('metaTags', metaTags.join("\n"), res);
			}

			/** Continue to next step of WebApp **/
			// this ensures that Meteor serves the standard page up

			next();

		}.bind(this));
		
	}

}