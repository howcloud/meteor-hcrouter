Route.prototype.eval = function (path, params) {

	if (this.action) {

		return this.action(params);

	} else if (this.redirect) {

		window.location = typeof this.redirect === 'function' ? this.redirect(params) : this.redirect;

		return true;
			
	/*} else if (this.reroute) {

		return this.reroute(params).eval(path, parmas);

	}*/

	} else if (this.rewrite) {

		var rewrite = this.getRewriteData(params);
		return rewrite.route.eval(path, rewrite.params); // we keep path the same as we only use it for analytics (see the controllerComponent section)

	} else if (this.controllerComponent) {

		/** Render **/

		var render = React.createElement(SiteController, {
			controller: this.controllerComponent, 
			controllerProps: params
		});

		ReactDOM.render(render, document.getElementById('react-root'));
		ReactMeteor.REACT_METEOR_INJECT_CHECK = false; // only check for injected data on initial render

		/** Page Analytics **/

		// TODO: Allow this data to be overriden at the controller level using a callback function in router
		//		 eg: trackPage or something which returns a page name, category etc (or just invokes the call itself...)

		if (window.analytics) {
			window.analytics.page(this.controllerComponent.displayName, params);
		} else {
			window.page_analyticsName = this.controllerComponent.displayName;
			window.page_analyticsData = params; // for triggering at a later time
		}

		/** Return Handled **/

		return true;

	}

	return false;

}