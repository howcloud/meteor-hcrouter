/**

Route
A representation of a static route rule which maps a path onto (eventually) a controller

**/

/*

options = {
	path: path to catch and route
	
	controllerComponent: // the react component controller to render
	or
	action: // evaluates a custom function (ie imagine api calls)
	or
	// DEPRECATED: reroute: // a function which returns a new router which should handle this call
	or
	redirect: // a new url to redirect the user to
	or
	rewrite: // rewrite the request to an underlying controller
}

*/

Route = function (router, options) {
	this.router = router;

	this.path = new Iron.Url(options.path);
	this.host = options.host; // allows us to specify a host space under which this route applies
							  // allows us to vary (for example) the page rendered as homepage depending on the subdomain the request is made from

	// should define one of these:
	this.action = options.action;
	this.controllerComponent = options.controllerComponent;
	this.rewrite = options.rewrite;
	this.redirect = options.redirect;

	// might define this on server side
	this._userCanAccess = options.userCanAccess;
	this._getMetaTags = options.getMetaTags;

}

Route.prototype.test = function (path) {
	return this.path.test(path);
}

Route.prototype.getUrlParams = function (url) {
	var props = {};

	var params = this.path.params(url); // for some reason this is an array with named properties, wtf. Some are simply indexes rather than keyed, we ignore them when constructing this lookup
	for (var x in params) props[x] = params[x]; // turned out to be the only way to iterate the screwed up format that iron router uses (see http://stackoverflow.com/questions/8630471/strings-as-keys-of-array-in-javascript)
	
	if (params.query) {
		_.extend(props, params.query);
	} else {
		props.query = {}; // always require this exists as an object
	}

	// delete props.query; // no need to duplicate this
						   // we now duplicate this so we pass in query params as an object directly to the controller

	return props;
}

/** Get Rewrite Data **/

// TODO: Change this up so that we don't have to constantly go via router.route when we want to lookup a path and do expensive regex etc
//		 We can instead have RouteInstances which are an instance of a client going to a particular page - they would contain the route, param, etc
//		 We'd then call eval on these

Route.prototype.getRewriteData = function (params) {
	if (!this.rewrite) return null;

	var _path = this.rewrite(params);
	var _route = this.router.findRoute(_path);
	var _params = _route.getUrlParams(_path)
	_.extend(_params.query, params.query); // we pass all query props through (note how right now original query props overwrite ones we have rewritten, technically. Maybe only pass through ones which don't exist in destination object?)

	return {
		path: _path,
		route: _route,
		params: _params
	}
}

/** Route Functionality **/
// Some of this is need to be controlled via a rewrite so we implement functions here to control it

Route.prototype.userCanAccess = function (user_id, params) {
	if (this._userCanAccess) return this._userCanAccess(user_id, params);

	var rewrite = this.getRewriteData(params);
	if (rewrite) return rewrite.route.userCanAccess(user_id, rewrite.params);

	return true;
}

Route.prototype.getMetaTags = function (params) {
	if (this._getMetaTags) {
		var metatags = _.clone(HCConfig.globalMetatags) || [];	
		return _.extend(metatags, this._getMetaTags(params));
	} else {
		var rewrite = this.getRewriteData(params);
		if (rewrite) {
			return rewrite.route.getMetaTags(rewrite.params);
		} else {
			return HCConfig.globalMetatags ? _.clone(HCConfig.globalMetatags) : null;
		}
	}
	
	return null;
}