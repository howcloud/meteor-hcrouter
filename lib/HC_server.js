HC = {

	web_url: function () {
		var _rv = RequestVars ? RequestVars.get() : null;
		if (!_rv || !_rv.req) return HCConfig.web_url;

		// We have a request so let's try and create the web_url from looking at the host which was requested and generated this request

		var headers = _rv.req.headers;
		
		var host = headers['x-forwarded-host'];
		if (!host) host = headers['host'];

		var proto = headers['x-forwarded-proto'];
		if (!proto) proto = 'http'; // should we assume secure ? probably.

		return proto + '://' + host + '/';
	},	

}