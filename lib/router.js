/**

Router

Manages a router, we instantiate a global version of this

**/

// MAKE THE ROUTER ITSELF A REACT COMPONENT?!?!?...
// The issue is that this is difficult on the server side
// We need this to CONTROL a react component... 

Router = function () {
	this.routes = [];

	Meteor.startup(function () {
		// Adding the setTimeout guarantees that this is always the last thing to be called, otherwise can end up in situation where some stuff required to render components on client are not defined (collections, for example)
		// Adding ANOTHER set timeout guarantees this will also be called after all the other 0 timeouts on startup are called.... a ridiculous hack which needs to be fixed by managing load orders by moving dependencies which require convoluted startup procedures like this to be defined in packages

		setTimeout(function () {
			setTimeout(function () {
				this.start();
			}.bind(this), 0);
		}.bind(this), 0);
	}.bind(this));
}

HC.Router = Router; // define as part of HC namespace

/** Routes **/

Router.prototype.route = function (options) {
	var route = new Route(this, options);
	this.routes.push(route);

	return route;
}

Router.prototype.findRoute = function (path, host) {
	// we match these by specificity
	// if a host is specified and we are able to pass a test on a route which matches that host then we use that route, if not we use the first one which passes our route test

	var foundForPath; // ie this will do if !host or we are not able to find a matched route with the right host, too
	var foundForHost;

	_.every(this.routes, function (route) {
		if (!foundForPath || !route.host || route.host === host) {
			if (route.test(path)) {
				foundForPath = route;

				if (route.host === host) {
					foundForHost = route;

					return false; // break - we have found the perfect match
				}

				return host ? true : false; // if there's a host - then continue looking for other paths until we find the perfect match (if it exists)
			}
		}

		return true;
	});

	return foundForHost ? foundForHost : foundForPath;
}

/** Extend **/

// Must be extended on server/client side to define:
// start: mechanism that starts the routing process